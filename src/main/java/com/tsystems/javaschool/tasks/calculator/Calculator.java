package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.*;

public class Calculator {


    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement){
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("#.####");
        decimalFormat.setDecimalFormatSymbols(dfs);
        if(statement==null||calculate(opn(statement)) == null)return null;
        else {
            try {
                return String.valueOf(decimalFormat.parse(calculate(opn(statement))));
            } catch (ParseException e) {
                return null;
            }
        }

    }
    private String opn(String sIn) {
        StringBuilder sbStack = new StringBuilder(""), sbOut = new StringBuilder("");
        char cIn, cTmp;
        if (sIn != null) {
            for (int i = 0; i < sIn.length(); i++) {
                cIn = sIn.charAt(i);
                if (isOp(cIn)) {
                    while (sbStack.length() > 0) {
                        cTmp = sbStack.substring(sbStack.length() - 1).charAt(0);
                        if (isOp(cTmp) && (opPrior(cIn) <= opPrior(cTmp))) {
                            sbOut.append(" ").append(cTmp).append(" ");
                            sbStack.setLength(sbStack.length() - 1);
                        } else {
                            sbOut.append(" ");
                            break;
                        }
                    }
                    sbOut.append(" ");
                    sbStack.append(cIn);
                } else if (',' == cIn) {
                    return null;
                } else if ('(' == cIn) {
                    sbStack.append(cIn);
                } else if (')' == cIn) {
                    cTmp = sbStack.substring(sbStack.length() - 1).charAt(0);
                    while ('(' != cTmp) {
                        if (sbStack.length() < 1) {
                            return null;
                        }
                        sbOut.append(" ").append(cTmp);
                        sbStack.setLength(sbStack.length() - 1);
                        cTmp = sbStack.substring(sbStack.length() - 1).charAt(0);
                    }
                    sbStack.setLength(sbStack.length() - 1);
                } else if(Character.isDigit(cIn)){
                    sbOut.append(cIn);
                }else if('.' == cIn){
                    sbOut.append(cIn);
                }else {
                    return null;
                }
            }

            while (sbStack.length() > 0) {
                sbOut.append(" ").append(sbStack.substring(sbStack.length() - 1));
                sbStack.setLength(sbStack.length() - 1);
            }

            return sbOut.toString();
        }else{
            return null;
        }
    }

    private boolean isOp(char c) {
        switch (c) {
            case '-':
            case '+':
            case '*':
            case '/':
                return true;
        }
        return false;
    }

    private byte opPrior(char op) {
        switch (op) {
            case '*':
            case '/':
            case '%':
                return 2;
        }
        return 1;
    }

    private String calculate(String sIn) {
        if(sIn!=null) {
            if (sIn.length() != 0) {
                double dA = 0, dB = 0;
                String sTmp;
                Deque<Double> stack = new ArrayDeque<Double>();
                StringTokenizer st = new StringTokenizer(sIn);
                while (st.hasMoreTokens()) {
                    sTmp = st.nextToken().trim();
                    if (1 == sTmp.length() && isOp(sTmp.charAt(0))) {
                        if (stack.size() < 2) {
                            return null;
                        }
                        dB = stack.pop();
                        dA = stack.pop();
                        switch (sTmp.charAt(0)) {
                            case '+':
                                dA += dB;
                                break;
                            case '-':
                                dA -= dB;
                                break;
                            case '/':
                                if (dB != 0) {
                                    dA /= dB;
                                    break;
                                } else {
                                    return null;
                                }

                            case '*':
                                dA *= dB;
                                break;
                            case '%':
                                dA %= dB;
                                break;
                            default:
                                return null;
                        }
                        stack.push(dA);
                    } else {
                        try {
                            dA = Double.parseDouble(sTmp);
                            stack.push(dA);
                        } catch (Exception e) {
                            return null;
                        }
                    }
                }

                if (stack.size() > 1) {
                    return null;
                }
                return String.valueOf(stack.pop());
            } else {
                return null;
            }
        }return null;
    }
}