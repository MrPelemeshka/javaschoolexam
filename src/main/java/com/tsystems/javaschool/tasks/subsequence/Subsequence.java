package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        StringBuilder sx = new StringBuilder();
        StringBuilder sy = new StringBuilder();
        if(x==null ||y==null){
            throw new IllegalArgumentException();
        }else {
            int flag = 0;
            for (Object value : x) {
                int count = 0;
                sx.append(value);

                for (int i = flag; i < y.size(); i++) {
                    if (value == y.get(i)) {
                        sy.append(y.get(i));
                        count++;
                        flag = i;
                        break;
                    }
                }
                if (count == 0) {
                    return false;
                }
            }
            System.out.println(sx);
            System.out.println(sy);
            return sx.toString().equals(sy.toString());
        }
    }
}
