package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class  PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int count;
        int lines = 1;
        int st = 1;
        try {
            Collections.sort(inputNumbers);
        } catch(Error | Exception e) {
            throw new CannotBuildPyramidException();
        }

        for(count=1;count<inputNumbers.size();count=count+lines){
            lines++;
            st=st+2;
        }
        if(inputNumbers.size()!=0&&count == inputNumbers.size()){
            int[][] pyramid = new int[lines][st];
            for(int i=0;i<lines;i++) {
                for (int j = 0; j < st; j++) {
                    pyramid[i][j] = 0;
                }
            }
            int k=-1;
            int t=0;
            int point= st/2;
            int l=0;
            for(int i=0;i<lines;i++){
                k++;
                l=k+1;
                for(int j =point - k;j<st&&l>0;j=j+2){
                    pyramid[i][j] = inputNumbers.get(t);
                    t++;
                    l--;
                }
            }
            return pyramid;
        }else{
            throw new CannotBuildPyramidException();
        }
    }
}
